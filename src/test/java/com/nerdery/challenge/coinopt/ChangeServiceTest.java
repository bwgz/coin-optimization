package com.nerdery.challenge.coinopt;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.nerdery.challenge.coinopt.core.Coin;
import com.nerdery.challenge.coinopt.service.ChangeService;

@ContextConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class ChangeServiceTest {
	@Autowired
	private ChangeService service;
	
	@Test
	public void test() {
	    Coin[] coins = { new Coin(100L), new Coin(50L), new Coin(25L), new Coin(10L), new Coin(5L), new Coin(1L) };
	    
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 0.01).toString(), "[Change [coin=Coin [value=1], quantity=1]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 0.11).toString(), "[Change [coin=Coin [value=10], quantity=1], Change [coin=Coin [value=1], quantity=1]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 0.16).toString(), "[Change [coin=Coin [value=10], quantity=1], Change [coin=Coin [value=5], quantity=1], Change [coin=Coin [value=1], quantity=1]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 0.21).toString(), "[Change [coin=Coin [value=10], quantity=2], Change [coin=Coin [value=1], quantity=1]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 1.41).toString(), "[Change [coin=Coin [value=100], quantity=1], Change [coin=Coin [value=25], quantity=1], Change [coin=Coin [value=10], quantity=1], Change [coin=Coin [value=5], quantity=1], Change [coin=Coin [value=1], quantity=1]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 1.97).toString(), "[Change [coin=Coin [value=100], quantity=1], Change [coin=Coin [value=50], quantity=1], Change [coin=Coin [value=25], quantity=1], Change [coin=Coin [value=10], quantity=2], Change [coin=Coin [value=1], quantity=2]]");
	    Assert.assertEquals(service.makeChange(Arrays.asList(coins), 2.01).toString(), "[Change [coin=Coin [value=100], quantity=2], Change [coin=Coin [value=1], quantity=1]]");
	}
}
