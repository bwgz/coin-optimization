<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Worlds Greatest Change Calculator</title>
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="jumbotron">
		<div class="container">
			<div class="row">
				<h1>Worlds Greatest Change Calculator</h1>
				<p>Calculate the optimal amount of change to return on a purchase.</p>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form class="form-horizontal" role="form" name="change" action="makeChange" method="POST">
				    <div class="form-group">
				      <label class="control-label col-sm-4" for="owed">Amount Owned:</label>
				      <div class="col-sm-5">
				        <input type="text" class="form-control" id="owed" placeholder="Enter amount owed" name="owed" value="<#if form.owed != 0>${form.owed?string["0.00"]}</#if>" required autofocus>
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="control-label col-sm-4" for="tendered">Amount Tendered:</label>
				      <div class="col-sm-5">          
				        <input type="text" class="form-control" id="tendered" placeholder="Enter amount tendered" name="tendered" value="<#if form.tendered != 0>${form.tendered?string["0.00"]}</#if>" required>
				      </div>
				    </div>
				    <div class="form-group">        
				      <div class="col-sm-offset-4 col-sm-5">
						<button class="btn btn-lg btn-primary btn-block" type="submit">Calculate Change</button>
				      </div>
				    </div>
				</form>
				
			</div>
			<div class="col-md-6">
				<h4>Change Due: ${due?string.currency}</h4>
				<#if change?size != 0 >
				<table class="table">
					<thead>
						<tr>
							<th>Coin</th>
							<th>Quantity</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<#assign total = 0> <#list change as item> <#assign value = item.coin.value / 100.0> <#assign subtotal = value * item.quantity>
					<#assign total = total + subtotal>
					<tr>
						<td>${value?string.currency}</td>
						<td>${item.quantity}</td>
						<td>${subtotal?string.currency}</td>
					</tr>
					</#list>
					<tr>
						<td colspan="2" align="right">Total:</td>
						<td>${total?string.currency}</td>
					</tr>
					</tbody>
				</table>
				</#if>
			</div>
		</div>
		<hr>

		<footer>
			<p>&copy; THE NERDERY, LLC 2015</p>
		</footer>
	</div>
	<!-- /container -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>