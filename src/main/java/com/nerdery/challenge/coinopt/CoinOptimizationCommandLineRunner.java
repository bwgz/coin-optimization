package com.nerdery.challenge.coinopt;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nerdery.challenge.coinopt.core.Coin;
import com.nerdery.challenge.coinopt.service.ChangeService;

@SpringBootApplication
public class CoinOptimizationCommandLineRunner implements CommandLineRunner {
	
	public static void main(String[] args) {
        SpringApplication.run(CoinOptimizationCommandLineRunner.class);
    }

	@Autowired
	private ChangeService service;

	@Override
    public void run(String... args) throws Exception {
	    System.out.println("time to test");
	    
	    Coin[] coins = { new Coin(100L), new Coin(50L), new Coin(25L), new Coin(10L), new Coin(5L), new Coin(1L) };
	    
	    for (int i = 0; i < 200; i++) {
	    	System.out.println(i + ": " + service.makeChange(Arrays.asList(coins), (i / 100.0)));
	    }
	}
}
