package com.nerdery.challenge.coinopt.service;

import java.math.BigDecimal;
import java.util.Collection;

import com.nerdery.challenge.coinopt.core.Change;
import com.nerdery.challenge.coinopt.core.Coin;

public interface ChangeService {
	Collection<Change> makeChange(Collection<Coin> coins, BigDecimal value);
	Collection<Change> makeChange(Collection<Coin> coins, Double value);
}
