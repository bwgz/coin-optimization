package com.nerdery.challenge.coinopt.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.nerdery.challenge.coinopt.core.Change;
import com.nerdery.challenge.coinopt.core.Coin;
import com.nerdery.challenge.coinopt.service.ChangeService;

@Service
public class ChangeServiceImpl implements ChangeService {
	private static Logger logger = LoggerFactory.getLogger(ChangeServiceImpl.class);
	
	private List<Coin> getHighestToLowest(List<Coin> coins) {
		Collections.sort(coins, new Comparator<Coin>() {

			@Override
			public int compare(Coin arg0, Coin arg1) {
				return (int) (arg1.getValue() - arg0.getValue());
			}
		});
		
		return coins;
	}
	
	private Change makeChange(Coin coin, Long value) {
		return new Change(coin, ((value >= coin.getValue()) ? value / coin.getValue() : 0));
	}
	
	/*
	 * This implementation uses a Greedy algorithm
	 * https://en.wikipedia.org/wiki/Greedy_algorithm
	 */
	private Collection<Change> makeChange(Collection<Coin> coins, Long value) {
		logger.debug("makeChange - coins: {}  value: {}", coins, value);
		Collection<Change> result = new ArrayList<Change>();
		
		for (Coin coin : getHighestToLowest(new ArrayList<Coin>(coins))) {
			Change change = makeChange(coin, value);
			
			if (change.getQuantity() != 0) {
				result.add(change);
				value -= change.getValue();
			}
		}
		
		return result;
	}

	@Override
	public Collection<Change> makeChange(Collection<Coin> coins, BigDecimal value) {
		logger.debug("makeChange - coins: {}  value: {}", coins, value);
		return makeChange(coins, value.scaleByPowerOfTen(2).longValue());
	}

	@Override
	public Collection<Change> makeChange(Collection<Coin> coins, Double value) {
		logger.debug("makeChange - coins: {}  value: {}", coins, value);
		return makeChange(coins, BigDecimal.valueOf(value));
	}
}
