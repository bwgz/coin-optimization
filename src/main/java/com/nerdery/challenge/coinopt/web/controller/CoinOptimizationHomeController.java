package com.nerdery.challenge.coinopt.web.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nerdery.challenge.coinopt.core.Change;
import com.nerdery.challenge.coinopt.core.Coin;
import com.nerdery.challenge.coinopt.service.ChangeService;
import com.nerdery.challenge.coinopt.web.controller.model.ChangeForm;

@Controller
public class CoinOptimizationHomeController {
	private static Logger logger = LoggerFactory.getLogger(CoinOptimizationHomeController.class);

	private static final Coin[] coins = { new Coin(100L), new Coin(50L), new Coin(25L), new Coin(10L), new Coin(5L), new Coin(1L) };
	private static final Collection<Change> NO_CHANGE = new ArrayList<Change>();

	@Autowired
	private ChangeService service;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("form", new ChangeForm());
		model.addAttribute("due", 0.00);
		model.addAttribute("change", NO_CHANGE);
		
		return "index";
	}
	
	@RequestMapping(value="/makeChange", method = RequestMethod.POST)
	public String makeChange(@ModelAttribute("change") ChangeForm form, Model model) {
		logger.debug("form: {}", form);
		
		BigDecimal due = form.getTendered().subtract(form.getOwed());
		logger.debug("due: {}", due);
		
		Collection<Change> change = due.doubleValue() > 0.0 ? service.makeChange(Arrays.asList(coins), due) : NO_CHANGE;
		logger.debug("change: {}", change);
		
		model.addAttribute("form", form);
		model.addAttribute("due", due.doubleValue());
		model.addAttribute("change", change);
		
		return "index";
	}
}
