package com.nerdery.challenge.coinopt.web.controller.model;

import java.math.BigDecimal;

public class ChangeForm {
	private BigDecimal owed = new BigDecimal(0);
	private BigDecimal tendered = new BigDecimal(0);
	
	public BigDecimal getOwed() {
		return owed;
	}
	
	public void setOwed(BigDecimal owned) {
		this.owed = owned;
	}
	
	public BigDecimal getTendered() {
		return tendered;
	}
	
	public void setTendered(BigDecimal tendered) {
		this.tendered = tendered;
	}
	
	@Override
	public String toString() {
		return "ChangeRequest [owned=" + owed + ", tendered=" + tendered + "]";
	}
	
}
