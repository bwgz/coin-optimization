package com.nerdery.challenge.coinopt.web.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nerdery.challenge.coinopt.core.Change;
import com.nerdery.challenge.coinopt.core.Coin;
import com.nerdery.challenge.coinopt.service.ChangeService;

@RestController()
@RequestMapping("/api/v1")
public class CoinOptimizationRestController {
	private static final  Coin[] coins = { new Coin(100L), new Coin(50L), new Coin(25L), new Coin(10L), new Coin(5L), new Coin(1L) };

	@Autowired
	private ChangeService service;
	
	@RequestMapping(value="change", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Collection<Change> makeChange(@RequestParam(value = "value", required = false) BigDecimal value) {
		return service.makeChange(Arrays.asList(coins), value);
	}
}
