package com.nerdery.challenge.coinopt.core;


public class Coin {
	private Long value;

	public Coin() {
	}
	
	public Coin(Long value) {
		this.value = value;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Coin [value=" + value + "]";
	}
}
