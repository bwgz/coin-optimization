package com.nerdery.challenge.coinopt.core;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Change {
	private Coin coin;
	private Long quantity;

	public Change() {
	}
	
	public Change(Coin coin, Long quantity) {
		this.coin = coin;
		this.quantity = quantity;
	}

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

    @JsonIgnore
	public Long getValue() {
		return coin.getValue() * quantity;
	}

	@Override
	public String toString() {
		return "Change [coin=" + coin + ", quantity=" + quantity + "]";
	}

}
