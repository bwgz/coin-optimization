
function Change(coin, quantity) {
    this.coin = coin;
    this.quantity = quantity;
}

function Coin(value) {
    this.value = value;
}

function makeChangeWithCoin(coin, value) {
    quantity = (value => coin.value) ? Math.floor(value / coin.value) : 0;

    return new Change(coin, quantity)
}

function makeChangeWithCoins(coins, value) {
    results = []
    for (i = 0; i < coins.length; i++) {
        change = makeChangeWithCoin(coins[i], value)
        results.push(change)
        value = value - (change.coin.value * change.quantity)
    }
    
    return results
}

var coins = Array(new Coin(100), new Coin(50), new Coin(25), new Coin(10), new Coin(5), new Coin(1));
var value = 6;

makeChangeWithCoins(coins, value)
